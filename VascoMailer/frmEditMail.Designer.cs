﻿namespace VascoMailer
{
    partial class VascoMailEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.htmlEditor = new System.Windows.Forms.WebBrowser();
            this.btnP = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.btnU = new System.Windows.Forms.Button();
            this.btnI = new System.Windows.Forms.Button();
            this.btnB = new System.Windows.Forms.Button();
            this.lstSize = new System.Windows.Forms.ComboBox();
            this.lstFont = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.colorDlg = new System.Windows.Forms.ColorDialog();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // htmlEditor
            // 
            this.htmlEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.htmlEditor.Location = new System.Drawing.Point(0, 123);
            this.htmlEditor.MinimumSize = new System.Drawing.Size(20, 20);
            this.htmlEditor.Name = "htmlEditor";
            this.htmlEditor.Size = new System.Drawing.Size(1000, 413);
            this.htmlEditor.TabIndex = 49;
            // 
            // btnP
            // 
            this.btnP.Location = new System.Drawing.Point(247, 10);
            this.btnP.Name = "btnP";
            this.btnP.Size = new System.Drawing.Size(88, 23);
            this.btnP.TabIndex = 48;
            this.btnP.Text = "Vstavi sliko";
            this.btnP.Visible = false;
            this.btnP.Click += new System.EventHandler(this.btnP_Click);
            // 
            // btnC
            // 
            this.btnC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnC.ForeColor = System.Drawing.Color.Red;
            this.btnC.Location = new System.Drawing.Point(90, 39);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(24, 23);
            this.btnC.TabIndex = 47;
            this.btnC.Text = "C";
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // btnU
            // 
            this.btnU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnU.Location = new System.Drawing.Point(60, 39);
            this.btnU.Name = "btnU";
            this.btnU.Size = new System.Drawing.Size(24, 23);
            this.btnU.TabIndex = 46;
            this.btnU.Text = "U";
            this.btnU.Click += new System.EventHandler(this.btnU_Click);
            // 
            // btnI
            // 
            this.btnI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnI.Location = new System.Drawing.Point(36, 39);
            this.btnI.Name = "btnI";
            this.btnI.Size = new System.Drawing.Size(24, 23);
            this.btnI.TabIndex = 45;
            this.btnI.Text = "I";
            this.btnI.Click += new System.EventHandler(this.btnI_Click);
            // 
            // btnB
            // 
            this.btnB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnB.Location = new System.Drawing.Point(12, 39);
            this.btnB.Name = "btnB";
            this.btnB.Size = new System.Drawing.Size(24, 23);
            this.btnB.TabIndex = 44;
            this.btnB.Text = "B";
            this.btnB.Click += new System.EventHandler(this.btnB_Click);
            // 
            // lstSize
            // 
            this.lstSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstSize.Location = new System.Drawing.Point(153, 12);
            this.lstSize.Name = "lstSize";
            this.lstSize.Size = new System.Drawing.Size(88, 21);
            this.lstSize.TabIndex = 43;
            this.lstSize.SelectedIndexChanged += new System.EventHandler(this.lstSize_SelectedIndexChanged);
            // 
            // lstFont
            // 
            this.lstFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstFont.Location = new System.Drawing.Point(11, 12);
            this.lstFont.Name = "lstFont";
            this.lstFont.Size = new System.Drawing.Size(136, 21);
            this.lstFont.TabIndex = 42;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(875, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 98);
            this.button1.TabIndex = 50;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(58, 88);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(593, 20);
            this.txtSubject.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "Naslov";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(153, 39);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(58, 23);
            this.button2.TabIndex = 53;
            this.button2.Text = "1 2 3";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(217, 39);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(24, 23);
            this.button3.TabIndex = 54;
            this.button3.Text = "*";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(264, 39);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(71, 23);
            this.button4.TabIndex = 55;
            this.button4.Text = "Link";
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // VascoMailEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 536);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.htmlEditor);
            this.Controls.Add(this.btnP);
            this.Controls.Add(this.btnC);
            this.Controls.Add(this.btnU);
            this.Controls.Add(this.btnI);
            this.Controls.Add(this.btnB);
            this.Controls.Add(this.lstSize);
            this.Controls.Add(this.lstFont);
            this.Name = "VascoMailEdit";
            this.RightToLeftLayout = true;
            this.Text = "Urejanje maila";
            this.Shown += new System.EventHandler(this.VascoMailEdit_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.WebBrowser htmlEditor;
        internal System.Windows.Forms.Button btnP;
        internal System.Windows.Forms.Button btnC;
        internal System.Windows.Forms.Button btnU;
        internal System.Windows.Forms.Button btnI;
        internal System.Windows.Forms.Button btnB;
        internal System.Windows.Forms.ComboBox lstSize;
        internal System.Windows.Forms.ComboBox lstFont;
        private System.Windows.Forms.Button button1;
        internal System.Windows.Forms.ColorDialog colorDlg;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Button button2;
        internal System.Windows.Forms.Button button3;
        public System.Windows.Forms.Button button4;
    }
}