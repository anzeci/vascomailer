﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IniParser;
using IniParser.Model;

namespace VascoMailer
{
    public partial class frmNastavitve : Form
    {
        public frmNastavitve()
        {
            InitializeComponent();
            LoadValues();


        }

        private void LoadValues()
        {
            FileIniDataParser ini = new FileIniDataParser();
            IniData data = ini.ReadFile("Settings.ini");
            txtNaziv.Text = data["Settings"]["Naziv"];
            txtEmail.Text = data["Settings"]["Email"];
            txtServer.Text = data["Settings"]["Server"];
            txtUser.Text = data["Settings"]["Uporabnik"];
            txtPassword.Text = data["Settings"]["Geslo"];
            txtPort.Value = Int32.Parse(data["Settings"]["Port"]);
        }

        private void btnPreklici_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPotrdi_Click(object sender, EventArgs e)
        {
            FileIniDataParser ini = new FileIniDataParser();
            IniData data = new IniData();// ini.ReadFile("Settings.ini");
            data["Settings"]["Naziv"] = txtNaziv.Text;
            data["Settings"]["Email"] = txtEmail.Text;
            data["Settings"]["Server"] = txtServer.Text;
            data["Settings"]["Uporabnik"] = txtUser.Text;
            data["Settings"]["Geslo"] = txtPassword.Text;
            data["Settings"]["Port"] = txtPort.Value.ToString();
            ini.WriteFile("Settings.ini", data);
            Close();
        }
    }
}
