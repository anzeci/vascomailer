﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static VascoIntegracija.Lib.EmailService;

namespace VascoMailer
{
    public partial class VascoMailEdit : Form
    {
        public mshtml.IHTMLDocument2 m_htmlDoc;
        public EmailPredloga Predloga;
        public VascoMailEdit()
        {
            InitializeComponent();
            htmlEditor.Navigate("about:blank");
            m_htmlDoc = htmlEditor.Document.DomDocument as mshtml.IHTMLDocument2;
            m_htmlDoc.designMode = "on";
            InitFonts();
        }


        private void InitFonts()
        { 
            string[] arFonts = new string[] {"Choose Font Style ...",
                  "Allegro BT",
                  "Arial",
                  "Arial Baltic",
                  "Arial Black",
                  "Arial CE",
                  "Arial CYR",
                  "Arial Greek",
                  "Arial Narrow",
                  "Arial TUR",
                  "AvantGarde Bk BT",
                  "BankGothic Md BT",
                  "Basemic",
                  "Basemic Symbol",
                  "Basemic Times",
                  "Batang",
                  "BatangChe",
                  "Benguiat Bk BT",
                  "BernhardFashion BT",
                  "BernhardMod BT",
                  "Book Antiqua",
                  "Bookman Old Style",
                  "Bremen Bd BT",
                  "Century Gothic",
                  "Charlesworth",
                  "Comic Sans MS",
                  "CommonBullets",
                  "CopprplGoth Bd BT",
                  "Courier",
                  "Courier New",
                  "Courier New Baltic",
                  "Courier New CE",
                  "Courier New CYR",
                  "Courier New Greek",
                  "Courier New TUR",
                  "Dauphin",
                  "Dotum",
                  "DotumChe",
                  "Dungeon",
                  "English111 Vivace BT",
                  "Estrangelo Edessa",
                  "Fixedsys",
                  "Franklin Gothic Medium",
                  "Futura Lt BT",
                  "Futura Md BT",
                  "Futura XBlk BT",
                  "FuturaBlack BT",
                  "Garamond",
                  "Gautami",
                  "Georgia",
                  "GoudyHandtooled BT",
                  "GoudyOlSt BT",
                  "Gulim",
                  "GulimChe",
                  "Gungsuh",
                  "GungsuhChe",
                  "Haettenschweiler",
                  "Humanst521 BT",
                  "Impact",
                  "Kabel Bk BT",
                  "Kabel Ult BT",
                  "Kingsoft Phonetic Plain",
                  "Latha",
                  "Lithograph",
                  "LithographLight",
                  "Lucida Console",
                  "Lucida Sans Unicode",
                  "Mangal",
                  "Marlett",
                  "Microsoft Sans Serif",
                  "MingLiU",
                  "Modern",
                  "Monotype Corsiva",
                  "MS Gothic",
                  "MS Mincho",
                  "MS Outlook",
                  "MS PGothic",
                  "MS PMincho",
                  "MS Sans Serif",
                  "MS Serif",
                  "MS UI Gothic",
                  "MT Extra",
                  "MV Boli",
                  "Myriad Condensed Web",
                  "Myriad Web",
                  "OzHandicraft BT",
                  "Palatino Linotype",
                  "PMingLiU",
                  "PosterBodoni BT",
                  "Raavi",
                  "Roman",
                  "Script",
                  "Serifa BT",
                  "Serifa Th BT",
                  "Shruti",
                  "Small Fonts",
                  "Souvenir Lt BT",
                  "Staccato222 BT",
                  "Swiss911 XCm BT",
                  "Sylfaen",
                  "Symbol",
                  "System",
                  "Tahoma",
                  "Terminal",
                  "Times New Roman",
                  "Times New Roman Baltic",
                  "Times New Roman CE",
                  "Times New Roman CYR",
                  "Times New Roman Greek",
                  "Times New Roman TUR",
                  "Trebuchet MS",
                  "Tunga",
                  "TypoUpright BT",
                  "Verdana",
                  "VisualUI",
                  "Webdings",
                  "Wingdings",
                  "Wingdings 2",
                  "Wingdings 3",
                  "WP Arabic Sihafa",
                  "WP ArabicScript Sihafa",
                  "WP BoxDrawing",
                  "WP CyrillicA",
                  "WP CyrillicB",
                  "WP Greek Century",
                  "WP Greek Courier",
                  "WP Greek Helve",
                  "WP Hebrew David",
                  "WP IconicSymbolsA",
                  "WP IconicSymbolsB",
                  "WP Japanese",
                  "WP MathA",
                  "WP MathB",
                  "WP MathExtendedA",
                  "WP MathExtendedB",
                  "WP MultinationalA Courier",
                  "WP MultinationalA Helve",
                  "WP MultinationalA Roman",
                  "WP MultinationalB Courier",
                  "WP MultinationalB Helve",
                  "WP MultinationalB Roman",
                  "WP Phonetic",
                  "WP TypographicSymbols",
                  "WSTCzec",
                  "WSTEngl",
                  "WSTFren",
                  "WSTGerm",
                  "WSTItal",
                  "WSTSpan",
                  "WSTSwed",
                  "ZapfEllipt BT",
                  "Zurich Ex BT"};

            int i;
            int nCount = arFonts.Length;
            for (i = 0; i <= nCount - 1; i++)
            {
                lstFont.Items.Add(arFonts[i]);
            }
            lstFont.SelectedIndex = 0;
            lstSize.Items.Add("Font Size ... ");
            for (i = 1; i <= 7; i++)
            {
                lstSize.Items.Add(i);
            }
            lstSize.SelectedIndex = 0;
         }

        private void btnB_Click(object sender, EventArgs e)
        {
            m_htmlDoc.execCommand("Bold", false);
            htmlEditor.Focus();
        }

        private void btnI_Click(object sender, EventArgs e)
        {
            m_htmlDoc.execCommand("Italic", false);
            htmlEditor.Focus();
        }

        private void btnU_Click(object sender, EventArgs e)
        {
            m_htmlDoc.execCommand("Underline", false);
            htmlEditor.Focus();
        }

        private void btnC_Click(object sender, EventArgs e)
        {
          if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                string v = String.Format("#{0:x2}{1:x2}{2:x2}", colorDlg.Color.R, colorDlg.Color.G, colorDlg.Color.B);
                m_htmlDoc.execCommand("ForeColor", false, v);
            }
            htmlEditor.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int start = m_htmlDoc.body.innerHTML.IndexOf("[odjava]");
            int stop = m_htmlDoc.body.innerHTML.IndexOf("[/odjava]");

            if (start >= stop) 
            {
                var confirmResult = MessageBox.Show("Nekje v dokumentu morate obvezo uporabiti link za odjavo. Primer [odjava]Odjava[/odjava]. Ali želite nadaljevati brez odjavne povezave?",
                                                     "Link za odjavo",
                                                      MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.No)
                    return;
            }

         
            Predloga.SPOROCILO = m_htmlDoc.body.innerHTML;
            Predloga.NASLOV = txtSubject.Text;
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void VascoMailEdit_Shown(object sender, EventArgs e)
        {
            m_htmlDoc.body.innerHTML = Predloga.SPOROCILO;
            txtSubject.Text = Predloga.NASLOV;
            htmlEditor.Focus();
        }

        private void btnP_Click(object sender, EventArgs e)
        {
            m_htmlDoc.execCommand("InsertImage", true);
        }

        private void lstSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            string size = lstSize.Text;
            lstSize.SelectedIndex = 0;
            m_htmlDoc.execCommand("fontsize", true, size);
            htmlEditor.Focus();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            m_htmlDoc.execCommand("InsertOrderedList", false);
            htmlEditor.Focus();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            m_htmlDoc.execCommand("InsertUnorderedList", false);
            htmlEditor.Focus();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmVpisiLink frm = new frmVpisiLink();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                m_htmlDoc.execCommand("createlink", false, frm.link);
                htmlEditor.Focus();
            }
    }


    }

    

}
