﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VascoMailer
{
    public partial class frmVpisiLink : Form
     {
        public string link;
        public frmVpisiLink()
        {
            InitializeComponent();
        }

        private void btnPotrdi_Click(object sender, EventArgs e)
        {
            link = txtLink.Text;
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnPreklici_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
