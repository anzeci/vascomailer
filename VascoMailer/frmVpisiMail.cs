﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VascoMailer
{   
    public partial class frmEmail : Form
    {
        public string email;
        public frmEmail()
        {
            InitializeComponent();
        }
        public void setEmail(string _email)
        {
            txtEmail.Text = _email;
        }

        private void btnPotrdi_Click(object sender, EventArgs e)
        {
            email = txtEmail.Text;
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnPreklici_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
