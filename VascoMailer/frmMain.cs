﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
//using System.Net.Mail;
using System.Net.Mime;
using VascoIntegracija.Lib;
using EASendMail;
using System.Runtime.InteropServices;
using DevExpress.Spreadsheet;
using IniParser;
using IniParser.Model;


namespace VascoMailer
{


    public partial class frmMain : Form
    {
        VascoIntegracija.Lib.EmailService EmailS;
        private mshtml.IHTMLDocument2 m_htmlDoc;
        private int CurrentSendingID;
        private string conStr = @"User=SYSDBA;Password=masterkey;Database=C:\baze\Vasco.FDB;DataSource=anzec-w10;
            Port=3050;Dialect=3;Charset=NONE;Role=;Connection lifetime=15;Pooling=true;
            MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=0;";

           /*private string conStr = @"User=SYSDBA;Password=vap0c0A21;Database=c:\Vasco\Vasco\vasco\1\Vasco.fdb;DataSource=192.168.1.10;
              Port=3050;Dialect=3;Charset=NONE;Role=;Connection lifetime=15;Pooling=true;
              MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=0;";*/

        public frmMain()
        {
            InitializeComponent();
            htmlEditor.Navigate("about:blank");
            m_htmlDoc = htmlEditor.Document.DomDocument as mshtml.IHTMLDocument2;
            // m_htmlDoc.designMode = "on";
            EmailS = new EmailService(conStr, Application.ExecutablePath);
            EmailS.Open();
            OdpriPredloge();
            timerInit.Enabled = true;
            CurrentSendingID = 0;
        }


        private void button1_Click(object sender, EventArgs e)
        {


        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void OdpriPredloge()
        {
            emailPredlogaBindingSource.DataSource = EmailS.VrniPredlogeList().ToList();
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {


            int id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            NaloziZapis(id);
            btnUredi.Tag = id;
            NaloziLog();


        }

        private EmailService.EmailPredloga NaloziZapis(int id)
        {
            if (!(id > 0))
            {
                return null;
            }

            EmailService.EmailPredloga Predloga = EmailS.DobiPredlogo(id);
            m_htmlDoc.body.innerHTML = Predloga.SPOROCILO;
            txtSubject.Text = Predloga.NASLOV;
            return Predloga;
        }

        private void btnUredi_Click(object sender, EventArgs e)
        {
            VascoMailEdit Edit = new VascoMailEdit();
            Edit.Predloga = EmailS.DobiPredlogo((int)btnUredi.Tag);
            int saveRow = dataGridView1.CurrentRow.Index;


            if (Edit.ShowDialog() == DialogResult.OK)
            {
                m_htmlDoc.body.innerHTML = Edit.m_htmlDoc.body.innerHTML;
                EmailS.ShraniPredlogo(Edit.Predloga);
                OdpriPredloge();
                if (saveRow != 0 && saveRow < dataGridView1.Rows.Count)
                {
                    dataGridView1.CurrentCell = dataGridView1.Rows[saveRow].Cells[0];
                }
                NaloziZapis((int)btnUredi.Tag);
            }
        }

        private void btnNov_Click(object sender, EventArgs e)
        {
            VascoMailEdit Edit = new VascoMailEdit();
            Edit.Predloga = EmailService.NovaPredloga();
            if (Edit.ShowDialog() == DialogResult.OK)
            {
                m_htmlDoc.body.innerHTML = Edit.m_htmlDoc.body.innerHTML;
                EmailS.ShraniPredlogo(Edit.Predloga);
                OdpriPredloge();
                NaloziLog();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (CurrentSendingID > 0)
            {
                timerSend.Enabled = false;
                btnSendAll.Text = "Pošlji vsem";
                CurrentSendingID = 0;
                return;
            }

            var confirmResult = MessageBox.Show("Ali ste prepričane da želite postali mail vsem našim strankam?",
                                                 "Potrditev pošiljanja",
                                                 MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.No)
            {
                return;
            }

            int id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value;
            progressBar1.Maximum = EmailS.VrniVseMaileCount();
            progressBar1.Value = EmailS.VrniObdelaneCount(id);
            timerSend.Enabled = true;
            btnSendAll.Text = "Pošiljanje mailov ...";
            CurrentSendingID = id;
        }

        private int RefreshProgress(int current)
        {
            progressBar1.Value = current;

            return current;
        }

        private void NaloziLog()
        {
            if (dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value == null)
            {
                return;
            }
            int id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value;
            var bs = new BindingList<EmailService.EmailLog>(EmailS.VrniLogList(id).ToList());
            emailLogBindingSource.DataSource = EmailS.VrniLogList(id).ToList();
            progressBar1.Maximum = EmailS.VrniVseMaileCount();
            progressBar1.Value = EmailS.VrniObdelaneCount(id);
            labelCount.Text = progressBar1.Value.ToString() + "/" + progressBar1.Maximum.ToString();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnPosljiSefu_Click(object sender, EventArgs e)
        {
            FileIniDataParser ini = new FileIniDataParser();
            IniData data = ini.ReadFile("Settings.ini");

            int id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value;
            frmEmail frm = new frmEmail();
            frm.setEmail(data["TestEmail"]["Email"]);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                data["TestEmail"]["Email"] = frm.email;
                ini.WriteFile("Settings.ini", data);
                EmailS.PosljiNaMail(id, frm.email);
            }
        }

        private void timerInit_Tick(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells[0];
            timerInit.Enabled = false;
            int id = (int)dataGridView1.Rows[0].Cells[0].Value;
            NaloziZapis(id);
            progressBar1.Maximum = EmailS.VrniVseMaileCount();
            progressBar1.Value = EmailS.VrniObdelaneCount(id);
            NaloziLog();
        }

        private void nastavitveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNastavitve f = new frmNastavitve();
            f.ShowDialog();
        }

        private void timerSend_Tick(object sender, EventArgs e)
        {
            int id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value;
            if (EmailS.PosljiNaslednjega(CurrentSendingID) == false)
            {
                timerSend.Enabled = false;
                CurrentSendingID = 0;
                btnSendAll.Text = "Pošlji vsem";

            }
            NaloziLog();
            progressBar1.Value = EmailS.VrniObdelaneCount(CurrentSendingID);
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            int id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value;
            NaloziZapis(id);
            btnUredi.Tag = id;
            NaloziLog();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value;
            NaloziZapis(id);
            btnUredi.Tag = id;
            NaloziLog();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void sdfdsfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value;
            NaloziZapis(id);
            VascoMailEdit Edit = new VascoMailEdit();
            Edit.Predloga = EmailS.KopirajPredlogo(id);
            if (Edit.ShowDialog() == DialogResult.OK)
            {
                m_htmlDoc.body.innerHTML = Edit.m_htmlDoc.body.innerHTML;
                EmailS.ShraniPredlogo(Edit.Predloga);
                OdpriPredloge();
                NaloziLog();
            }
        }

        private void sdfdsfToolStripMenuItem_DoubleClick(object sender, EventArgs e)
        {

        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                contextMenuStrip1.Show(dataGridView1, e.X, e.Y);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }



        private void button1_Click_2(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Excel |*.xls;*.xlsx";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (openFileDialog1.FileName.Length > 0)
                    {
                     //   VascoIntegracija.Lib.TReadExcelRangeResult
                        TReadExcelRangeResult ReadExcelRangeResult = VascoIntegracija.Lib.ExcelService.GetExcelUsedRangeFromSheet1(openFileDialog1.FileName);
                        string str;
                        int rCnt;
                        int cCnt;
                        for (rCnt = 0; rCnt < dataGridView3.Rows.Count; rCnt++)
                        {
                            dataGridView3.Rows.Clear();
                        }
                        for (rCnt = 1; rCnt < ReadExcelRangeResult.Rows; rCnt++)
                        {
                            for (cCnt = 0; cCnt < ReadExcelRangeResult.Columns; cCnt++)
                            {
                                str = ReadExcelRangeResult.Data[rCnt, cCnt].Value.ToString();
                                if (str != null && str.Contains("@"))
                                    dataGridView3.Rows.Add(str);
                            }
                        }
                        lblCountXLS.Text = "0/" + (dataGridView3.Rows.Count - 1).ToString();
                        progressBar2.Maximum = dataGridView3.Rows.Count - 1;
                        progressBar2.Value = 0; 
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

    private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void timerSendXLS_Tick(object sender, EventArgs e)
        {
            timerSendXLS.Enabled = false;
            btnSendAllXLS.Text = "Pošlji vsem";
            for (int rCnt = 0; rCnt < dataGridView3.Rows.Count; rCnt++)
            {

                if ((dataGridView3.Rows[rCnt].Cells["EMAIL"].Value != null) && (Convert.ToBoolean(dataGridView3.Rows[rCnt].Cells["POSLANO"].Value) != true))
                {
                    progressBar2.Value = rCnt+1;
                    lblCountXLS.Text = progressBar2.Value +"/"+ (dataGridView3.Rows.Count - 1).ToString();
                    int id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value;
                    EmailS.PosljiNaMail(id, dataGridView3.Rows[rCnt].Cells["EMAIL"].Value.ToString());
                    dataGridView3.Rows[rCnt].Cells["POSLANO"].Value = true;
                    timerSendXLS.Enabled = true;
                    btnSendAllXLS.Text = "Pošiljanje mailov ...";
                    return;

                }

            }

            /*  {
                  int id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value;
                  if (EmailS.PosljiNaslednjega(CurrentSendingID) == false)
                  {
                      timerSendXLS.Enabled = false;
                      CurrentSendingID = 0;
                      btnSendAllXLS.Text = "Pošlji vsem";

                  }
                  NaloziLog();
                  progressBar2.Value = EmailS.VrniObdelaneCount(CurrentSendingID);
              }*/

        }

        private void btnSendAllXLS_Click(object sender, EventArgs e)
        {

            if (timerSendXLS.Enabled)
            {
                timerSendXLS.Enabled = false;
                btnSendAllXLS.Text = "Pošlji vsem";
                return;
            }

            var confirmResult = MessageBox.Show("Ali ste prepričane da želite postali mail vsem iz seznama?",
                                                 "Potrditev pošiljanja",
                                                 MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.No)
            {
                return;
            }

            timerSendXLS.Enabled = true;
            btnSendAllXLS.Text = "Pošiljanje mailov ...";
        }

        private void btnBrisi_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Ali ste prepričani, da želite brisati predlogo?",
                                               "Potrditev brisanja",
                                               MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                EmailS.BrisiZapis((int)btnUredi.Tag);
                OdpriPredloge();
                NaloziLog();
            }

        }
    }
}
