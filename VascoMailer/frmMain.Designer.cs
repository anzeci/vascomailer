﻿namespace VascoMailer
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.htmlEditor = new System.Windows.Forms.WebBrowser();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnNov = new System.Windows.Forms.Button();
            this.btnUredi = new System.Windows.Forms.Button();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnSendAll = new System.Windows.Forms.Button();
            this.labelCount = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.eMAILDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cASPOSILJANJADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uSPESNODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.emailLogBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnPosljiSefu = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnSendAllXLS = new System.Windows.Forms.Button();
            this.lblCountXLS = new System.Windows.Forms.Label();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POSLANO = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.aUTOIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nASLOVDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sPOROCILODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailPredlogaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.timerInit = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nastavitveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerSend = new System.Windows.Forms.Timer(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sdfdsfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerSendXLS = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnBrisi = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailLogBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailPredlogaBindingSource)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 246);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1250, 357);
            this.panel1.TabIndex = 45;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.htmlEditor);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 41);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1250, 316);
            this.panel5.TabIndex = 57;
            // 
            // htmlEditor
            // 
            this.htmlEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.htmlEditor.Location = new System.Drawing.Point(0, 0);
            this.htmlEditor.MinimumSize = new System.Drawing.Size(20, 20);
            this.htmlEditor.Name = "htmlEditor";
            this.htmlEditor.Size = new System.Drawing.Size(1250, 316);
            this.htmlEditor.TabIndex = 45;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnBrisi);
            this.panel4.Controls.Add(this.btnNov);
            this.panel4.Controls.Add(this.btnUredi);
            this.panel4.Controls.Add(this.txtSubject);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1250, 41);
            this.panel4.TabIndex = 56;
            // 
            // btnNov
            // 
            this.btnNov.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNov.Location = new System.Drawing.Point(883, 7);
            this.btnNov.Name = "btnNov";
            this.btnNov.Size = new System.Drawing.Size(75, 23);
            this.btnNov.TabIndex = 59;
            this.btnNov.Text = "Nov";
            this.btnNov.UseVisualStyleBackColor = true;
            this.btnNov.Click += new System.EventHandler(this.btnNov_Click);
            // 
            // btnUredi
            // 
            this.btnUredi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUredi.Location = new System.Drawing.Point(802, 7);
            this.btnUredi.Name = "btnUredi";
            this.btnUredi.Size = new System.Drawing.Size(75, 23);
            this.btnUredi.TabIndex = 58;
            this.btnUredi.Tag = "0";
            this.btnUredi.Text = "Uredi";
            this.btnUredi.UseVisualStyleBackColor = true;
            this.btnUredi.Click += new System.EventHandler(this.btnUredi_Click);
            // 
            // txtSubject
            // 
            this.txtSubject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubject.Location = new System.Drawing.Point(60, 7);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.ReadOnly = true;
            this.txtSubject.Size = new System.Drawing.Size(736, 20);
            this.txtSubject.TabIndex = 56;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.TabIndex = 57;
            this.label1.Text = "Naslov";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.splitter1);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 24);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1250, 603);
            this.panel3.TabIndex = 56;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.splitter1.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 243);
            this.splitter1.Name = "splitter1";
            this.splitter1.Padding = new System.Windows.Forms.Padding(2);
            this.splitter1.Size = new System.Drawing.Size(1250, 3);
            this.splitter1.TabIndex = 46;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1250, 243);
            this.panel2.TabIndex = 49;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(565, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(682, 234);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnSendAll);
            this.tabPage1.Controls.Add(this.labelCount);
            this.tabPage1.Controls.Add(this.progressBar1);
            this.tabPage1.Controls.Add(this.dataGridView2);
            this.tabPage1.Controls.Add(this.btnPosljiSefu);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(674, 208);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Pošiljanje iz baze";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnSendAll
            // 
            this.btnSendAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSendAll.Image = global::VascoMailer.Properties.Resources.send;
            this.btnSendAll.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSendAll.Location = new System.Drawing.Point(532, 3);
            this.btnSendAll.Name = "btnSendAll";
            this.btnSendAll.Size = new System.Drawing.Size(128, 159);
            this.btnSendAll.TabIndex = 53;
            this.btnSendAll.Text = "Pošlji vsem";
            this.btnSendAll.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSendAll.UseVisualStyleBackColor = true;
            this.btnSendAll.Click += new System.EventHandler(this.button3_Click);
            // 
            // labelCount
            // 
            this.labelCount.AutoSize = true;
            this.labelCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCount.Location = new System.Drawing.Point(431, 189);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(51, 16);
            this.labelCount.TabIndex = 58;
            this.labelCount.Text = "Naslov";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(-15, 189);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(422, 14);
            this.progressBar1.TabIndex = 55;
            this.progressBar1.Value = 100;
            // 
            // dataGridView2
            // 
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.eMAILDataGridViewTextBoxColumn,
            this.cASPOSILJANJADataGridViewTextBoxColumn,
            this.uSPESNODataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.emailLogBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(-15, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(422, 182);
            this.dataGridView2.TabIndex = 52;
            // 
            // eMAILDataGridViewTextBoxColumn
            // 
            this.eMAILDataGridViewTextBoxColumn.DataPropertyName = "EMAIL";
            this.eMAILDataGridViewTextBoxColumn.HeaderText = "Email";
            this.eMAILDataGridViewTextBoxColumn.Name = "eMAILDataGridViewTextBoxColumn";
            this.eMAILDataGridViewTextBoxColumn.ReadOnly = true;
            this.eMAILDataGridViewTextBoxColumn.Width = 150;
            // 
            // cASPOSILJANJADataGridViewTextBoxColumn
            // 
            this.cASPOSILJANJADataGridViewTextBoxColumn.DataPropertyName = "CAS_POSILJANJA";
            this.cASPOSILJANJADataGridViewTextBoxColumn.HeaderText = "Poslano";
            this.cASPOSILJANJADataGridViewTextBoxColumn.Name = "cASPOSILJANJADataGridViewTextBoxColumn";
            this.cASPOSILJANJADataGridViewTextBoxColumn.ReadOnly = true;
            this.cASPOSILJANJADataGridViewTextBoxColumn.Width = 150;
            // 
            // uSPESNODataGridViewTextBoxColumn
            // 
            this.uSPESNODataGridViewTextBoxColumn.DataPropertyName = "USPESNO";
            this.uSPESNODataGridViewTextBoxColumn.FalseValue = "0";
            this.uSPESNODataGridViewTextBoxColumn.HeaderText = "Uspešno";
            this.uSPESNODataGridViewTextBoxColumn.Name = "uSPESNODataGridViewTextBoxColumn";
            this.uSPESNODataGridViewTextBoxColumn.ReadOnly = true;
            this.uSPESNODataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.uSPESNODataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.uSPESNODataGridViewTextBoxColumn.TrueValue = "1";
            this.uSPESNODataGridViewTextBoxColumn.Width = 60;
            // 
            // emailLogBindingSource
            // 
            this.emailLogBindingSource.DataSource = typeof(VascoIntegracija.Lib.EmailService.EmailLog);
            // 
            // btnPosljiSefu
            // 
            this.btnPosljiSefu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPosljiSefu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPosljiSefu.Image = global::VascoMailer.Properties.Resources.test;
            this.btnPosljiSefu.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPosljiSefu.Location = new System.Drawing.Point(411, 3);
            this.btnPosljiSefu.Name = "btnPosljiSefu";
            this.btnPosljiSefu.Size = new System.Drawing.Size(120, 159);
            this.btnPosljiSefu.TabIndex = 54;
            this.btnPosljiSefu.Text = "Pošlji test";
            this.btnPosljiSefu.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPosljiSefu.UseVisualStyleBackColor = true;
            this.btnPosljiSefu.Click += new System.EventHandler(this.btnPosljiSefu_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnSendAllXLS);
            this.tabPage2.Controls.Add(this.lblCountXLS);
            this.tabPage2.Controls.Add(this.progressBar2);
            this.tabPage2.Controls.Add(this.dataGridView3);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(674, 208);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Pošiljanje iz Excela";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnSendAllXLS
            // 
            this.btnSendAllXLS.Location = new System.Drawing.Point(475, 49);
            this.btnSendAllXLS.Name = "btnSendAllXLS";
            this.btnSendAllXLS.Size = new System.Drawing.Size(137, 39);
            this.btnSendAllXLS.TabIndex = 65;
            this.btnSendAllXLS.Text = "Pošlji";
            this.btnSendAllXLS.UseVisualStyleBackColor = true;
            this.btnSendAllXLS.Click += new System.EventHandler(this.btnSendAllXLS_Click);
            // 
            // lblCountXLS
            // 
            this.lblCountXLS.AutoSize = true;
            this.lblCountXLS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCountXLS.Location = new System.Drawing.Point(472, 183);
            this.lblCountXLS.Name = "lblCountXLS";
            this.lblCountXLS.Size = new System.Drawing.Size(0, 16);
            this.lblCountXLS.TabIndex = 64;
            // 
            // progressBar2
            // 
            this.progressBar2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar2.Location = new System.Drawing.Point(3, 185);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(466, 14);
            this.progressBar2.TabIndex = 63;
            this.progressBar2.Value = 100;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EMAIL,
            this.POSLANO});
            this.dataGridView3.Location = new System.Drawing.Point(0, 0);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(469, 179);
            this.dataGridView3.TabIndex = 62;
            this.dataGridView3.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellContentClick);
            // 
            // EMAIL
            // 
            this.EMAIL.HeaderText = "Email";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.Width = 300;
            // 
            // POSLANO
            // 
            this.POSLANO.HeaderText = "Poslano";
            this.POSLANO.Name = "POSLANO";
            this.POSLANO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.POSLANO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(475, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 40);
            this.button1.TabIndex = 61;
            this.button1.Text = "Uvozi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.aUTOIDDataGridViewTextBoxColumn,
            this.nASLOVDataGridViewTextBoxColumn,
            this.sPOROCILODataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.emailPredlogaBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(556, 234);
            this.dataGridView1.TabIndex = 49;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseClick);
            this.dataGridView1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dataGridView1_Scroll);
            this.dataGridView1.Click += new System.EventHandler(this.dataGridView1_Click);
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseClick);
            // 
            // aUTOIDDataGridViewTextBoxColumn
            // 
            this.aUTOIDDataGridViewTextBoxColumn.DataPropertyName = "AUTO_ID";
            this.aUTOIDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.aUTOIDDataGridViewTextBoxColumn.Name = "aUTOIDDataGridViewTextBoxColumn";
            this.aUTOIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.aUTOIDDataGridViewTextBoxColumn.Width = 80;
            // 
            // nASLOVDataGridViewTextBoxColumn
            // 
            this.nASLOVDataGridViewTextBoxColumn.DataPropertyName = "NASLOV";
            this.nASLOVDataGridViewTextBoxColumn.HeaderText = "Naslov";
            this.nASLOVDataGridViewTextBoxColumn.Name = "nASLOVDataGridViewTextBoxColumn";
            this.nASLOVDataGridViewTextBoxColumn.ReadOnly = true;
            this.nASLOVDataGridViewTextBoxColumn.Width = 380;
            // 
            // sPOROCILODataGridViewTextBoxColumn
            // 
            this.sPOROCILODataGridViewTextBoxColumn.DataPropertyName = "SPOROCILO";
            this.sPOROCILODataGridViewTextBoxColumn.HeaderText = "SPOROCILO";
            this.sPOROCILODataGridViewTextBoxColumn.Name = "sPOROCILODataGridViewTextBoxColumn";
            this.sPOROCILODataGridViewTextBoxColumn.ReadOnly = true;
            this.sPOROCILODataGridViewTextBoxColumn.Visible = false;
            // 
            // emailPredlogaBindingSource
            // 
            this.emailPredlogaBindingSource.DataSource = typeof(VascoIntegracija.Lib.EmailService.EmailPredloga);
            // 
            // timerInit
            // 
            this.timerInit.Tick += new System.EventHandler(this.timerInit_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nastavitveToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1250, 24);
            this.menuStrip1.TabIndex = 57;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // nastavitveToolStripMenuItem
            // 
            this.nastavitveToolStripMenuItem.Name = "nastavitveToolStripMenuItem";
            this.nastavitveToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.nastavitveToolStripMenuItem.Text = "Nastavitve";
            this.nastavitveToolStripMenuItem.Click += new System.EventHandler(this.nastavitveToolStripMenuItem_Click);
            // 
            // timerSend
            // 
            this.timerSend.Interval = 1000;
            this.timerSend.Tick += new System.EventHandler(this.timerSend_Tick);
            // 
            // bindingSource1
            // 
            this.bindingSource1.CurrentChanged += new System.EventHandler(this.bindingSource1_CurrentChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sdfdsfToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(202, 26);
            // 
            // sdfdsfToolStripMenuItem
            // 
            this.sdfdsfToolStripMenuItem.Name = "sdfdsfToolStripMenuItem";
            this.sdfdsfToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.sdfdsfToolStripMenuItem.Text = "Kopiraj predlogo v novo";
            this.sdfdsfToolStripMenuItem.Click += new System.EventHandler(this.sdfdsfToolStripMenuItem_Click);
            this.sdfdsfToolStripMenuItem.DoubleClick += new System.EventHandler(this.sdfdsfToolStripMenuItem_DoubleClick);
            // 
            // timerSendXLS
            // 
            this.timerSendXLS.Interval = 1000;
            this.timerSendXLS.Tick += new System.EventHandler(this.timerSendXLS_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnBrisi
            // 
            this.btnBrisi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrisi.Location = new System.Drawing.Point(1163, 7);
            this.btnBrisi.Name = "btnBrisi";
            this.btnBrisi.Size = new System.Drawing.Size(75, 23);
            this.btnBrisi.TabIndex = 60;
            this.btnBrisi.Text = "Briši";
            this.btnBrisi.UseVisualStyleBackColor = true;
            this.btnBrisi.Click += new System.EventHandler(this.btnBrisi_Click);
            // 
            // frmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1250, 627);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "Vasco mailer";
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailLogBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailPredlogaBindingSource)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.WebBrowser htmlEditor;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnNov;
        private System.Windows.Forms.Button btnUredi;
        private System.Windows.Forms.BindingSource emailPredlogaBindingSource;
        private System.Windows.Forms.BindingSource emailLogBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn aUTOIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nASLOVDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sPOROCILODataGridViewTextBoxColumn;
        private System.Windows.Forms.Timer timerInit;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nastavitveToolStripMenuItem;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer timerSend;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sdfdsfToolStripMenuItem;
        private System.Windows.Forms.Label labelCount;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnSendAll;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn eMAILDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cASPOSILJANJADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn uSPESNODataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnPosljiSefu;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Label lblCountXLS;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL;
        private System.Windows.Forms.DataGridViewCheckBoxColumn POSLANO;
        private System.Windows.Forms.Timer timerSendXLS;
        private System.Windows.Forms.Button btnSendAllXLS;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnBrisi;
    }
}

